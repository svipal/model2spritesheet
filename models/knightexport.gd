tool

extends Spatial

var shaderMaterial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

func get_mesh_instance():
    return $Character/Skeleton/KnightMesh

func set_normal_map(value):
    print("normal_map:",value)
    if value:
        get_mesh_instance().set_surface_material(0,shaderMaterial)
#        $Character/Skeleton/KnightMesh.get_surface_material(0).set_shader_param("normal_map",value)
    else:
        get_mesh_instance().set_surface_material(0,null)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
