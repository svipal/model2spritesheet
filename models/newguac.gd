tool
extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var shaderMaterial

func get_mesh_instance():
    return $metarig/Skeleton/Plane

# Called when the node enters the scene tree for the first time.
func _ready():
    $metarig/Skeleton/Plane.get_surface_material(0).set_shader_param("normal_map", false)
    $AnimationPlayer.play("charge")
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
  pass



func set_normal_map(value):
    print("normal_map:",value)
    if value:
        get_mesh_instance().set_surface_material(0,shaderMaterial)
#        $Character/Skeleton/KnightMesh.get_surface_material(0).set_shader_param("normal_map",value)
    else:
        get_mesh_instance().set_surface_material(0,null)
        


func change_frame(frame: float,max_frames: float):
  var max_time = $AnimationPlayer.current_animation_length
  var wished_time = (frame/max_frames)*max_time
#  print(wished_time)
  $AnimationPlayer.seek(wished_time, true)

