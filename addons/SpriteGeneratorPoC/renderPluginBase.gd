tool
extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var animations = {}
var models = {}
# Called when the node enters the scene tree for the first time.

func reset_models():
    for i in range(0,$Options/Models.get_item_count()):
        $Options/Models.remove_item(0)
    

func reset_animations():
    
    for i in range(0,$Options/Animations.get_item_count()):
        $Options/Animations.remove_item(0)

func build_animations():
    var i = 0
    for animation in current_model().get_node("AnimationPlayer").get_animation_list():
        $Options/Animations.add_item(animation,i)
        animations[i]=animation
        i += 1

func current_model():
    print (models)
    var modelid = $Options/Models.selected
    return models[modelid]

func _ready():
    print("loading shader")
    var material = ShaderMaterial.new()
    var normal_map_shader: Shader = preload("res://addons/SpriteGeneratorPoC/normal_map.shader")
    material.set_shader(normal_map_shader)
    for model in $Internal/Viewport/Models.get_children():
        model.shaderMaterial = material
#        model.get_mesh_instance().set_surface_material(0,material)
    
    print("ready")
    #clean up
    reset_models()
    reset_animations()

    print("clean")      
    
    #link viewports together
    var world = $Internal/Viewport.find_world()
    $ViewportContainer/Viewport.set_world(world)
    
    var i  = 0
    for model in $Internal/Viewport/Models.get_children():
        $Options/Models.add_item(model.name,i)
        print(model.name)
        print("current number of models:",$Options/Models.get_item_count())
        models[i]=model
        i += 1
    $Options/Models.select(0)
    select_model(0)
    build_animations()
        # c'est pas fini ^
    
    


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#  pass

var max_frames = 12


func renderToPic(totalData,name):
    print("start !")
    var width  = $Internal/Viewport.size.x
    var height = $Internal/Viewport.size.y
    print("w:",width,"\nh:",height)
    var format = $Internal/Viewport.get_texture().get_data().get_format()
    totalData.create(width*max_frames,height*8,false,format)
    totalData.fill(Color(1,0,0,1))
    totalData.unlock()
    for i in range(0,8):
        print("direction:", i)
        for frame in range(0,max_frames):
            print("frame:",frame)
            current_model().change_frame(float(frame),float(max_frames))
            #ask for the good frame, wait for the sync
            yield(get_tree(), "idle_frame")
            yield(get_tree(), "idle_frame")
            # get the texture
            var current_data = $Internal/Viewport.get_texture().get_data()
            current_data.flip_y()
            #paste the texture in the current texture
            totalData.blit_rect(current_data,Rect2(Vector2(0,0),Vector2(width,height)),Vector2(frame*width,i*height))
            #update position
        $Internal/Viewport/Spatial.rotate_y(PI/4)
    totalData.lock()
    totalData.save_png("exports/"+name+".png")
    print("finished !")


func _on_RenderButton_pressed():
    var sheetData       = Image.new()
    renderToPic(sheetData,"test")
    
    yield(get_tree(), "idle_frame")
    yield(get_tree(), "idle_frame")


func _on_Animations_item_selected(id):
    # we pla
    current_model().get_node("AnimationPlayer").play(animations[id])
    pass # Replace with function body.
    


func select_model(id):
    print ($Options/Models.get_item_count())
    # we only show the selected model
    for child in $Internal/Viewport/Models.get_children():
        child.visible = false
    current_model().visible=true
    reset_animations()
    build_animations()


func _on_Models_item_selected(id):
    select_model(id)


func _on_NormalButton_toggled(button_pressed):
    print("owowo")
    current_model().set_normal_map(button_pressed)
