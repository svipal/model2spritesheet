shader_type spatial;

varying vec3 some_color;
uniform bool normal_map;

void vertex() {
    some_color = NORMAL; // make the normal the color
}

void fragment() {
    ALBEDO = NORMAL *0.5 + vec3(0.5);
}