extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum Directions {
    RIGHT,
    DOWN_RIGHT,
    DOWN,
    DOWN_LEFT,
    LEFT,
    UP_LEFT,
    UP,
    UP_RIGHT
}

enum State {
    RUN,
    DASH,
    PUNCH
}
var direction = Directions.UP
var state = State.RUN
var animationPlayer = null
# Called when the node enters the scene tree for the first time.
func _ready():
    animationPlayer = $Sprite/AnimationPlayer
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
func refresh_animation():
    var dir_4way = int(direction)
    if state == State.RUN:
        animationPlayer.play("Run" + str(dir_4way))
    if state == State.PUNCH:
        animationPlayer.play("Punch" + str(dir_4way))
    if state == State.DASH:
        animationPlayer.play("Dash" + str(dir_4way))
               
func new_orientation(dir):
    print("New ori " + str(dir))
    direction = dir
    refresh_animation()

func _on_TouchManager_drag(n_side):
    var tmp_dir4way = Directions.UP
    if n_side == 0 or n_side == 1 or n_side == 7:
        tmp_dir4way = Directions.RIGHT
    elif n_side == 3 or n_side == 4 or n_side == 5:
        tmp_dir4way = Directions.LEFT
    elif n_side == 2:
        tmp_dir4way = Directions.DOWN
    elif n_side == 6:
        tmp_dir4way = Directions.UP
    else:
        print('ERROR')
    if not direction == tmp_dir4way:
        new_orientation(tmp_dir4way)
    pass # Replace with function body.


func _on_TouchManager_release():
    pass # Replace with function body.


func _on_TouchManager_swipe(direction):
    pass # Replace with function body.
