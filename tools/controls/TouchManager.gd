extends Node

signal swipe(direction)
signal swipe_canceled()
signal drag(n_side)
signal release()

export (int, 10, 1000) var SWIPE_MAX_TIME_MS = 400
export (float, 10, 200) var SWIPE_MIN_DISTANCE = 50
export (int, 10, 100) var SWIPE_MIN_TIME_MS = 50

onready var timer = $Timer
var swipe_start_pos = Vector2()
var start_ms = null
var drag_direction = null 

func _input(event):
    if event is InputEventScreenTouch:
        if event.pressed:
            _start_detection(event.position)
        elif not timer.is_stopped():
            $DebugLine2D.clear_points()
            _end_detection(event.position)
    if event is InputEventScreenDrag:
        $DebugLine2D.clear_points()
        $DebugLine2D.add_point(swipe_start_pos)
        $DebugLine2D.add_point(event.position)
        var dir = (event.position - swipe_start_pos).normalized()
        var dir2 = Vector2(event.relative.x, event.relative.y).normalized()
        var rads = atan2(-dir.y, dir.x)
        
        var newrads = rads
        if rads < 0:
            newrads = PI + PI + rads
        #print(dir, " ", rads, " ", newrads)
        # ORIGIN SHIFT
        if newrads >= PI / 8:
            newrads = newrads - PI / 8
        if newrads < PI / 8:
            newrads = 2 * PI - newrads
        # DIR COMPUTE
        var n = 7 - floor(((newrads) / (2 * PI)) * 8)
        if drag_direction != n:
            #print(rads)
            #print("New DragDir: " + str(n))
            drag_direction = n
            if n < 0:
                n = 0
            if n > 7:
                n = 7
            emit_signal("drag", drag_direction)
        

func _start_detection(position):
    swipe_start_pos = position
    timer.start()
    start_ms = OS.get_ticks_msec()

func _end_detection(position):
    timer.stop()
    var end_ms = OS.get_ticks_msec()
    var duration = end_ms - start_ms
    var direction = (position - swipe_start_pos).normalized()
    if abs((position - swipe_start_pos).length()) > SWIPE_MIN_DISTANCE \
       and duration < SWIPE_MAX_TIME_MS \
       and duration > SWIPE_MIN_TIME_MS:
        print("swipe", direction)
        emit_signal("swipe", direction)
    emit_signal("release")
    print("release")


    
func _on_Timer_timeout():
    emit_signal("swipe_canceled", swipe_start_pos)
    pass # Replace with function body.

func _ready():
    pass # Replace with function body.
